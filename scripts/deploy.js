async function main() {
  const VEarthNft = await ethers.getContractFactory('VEarthNft');

  const _VEarthNft = await VEarthNft.deploy();

  console.log('Contract deployed at address : ', _VEarthNft.address);
}
main()
  .then(() => process.exit(0))
  .catch(e => {
    console.log(e);
    process.exit(0);
  });
