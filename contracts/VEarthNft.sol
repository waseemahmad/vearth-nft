// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721URIStorage.sol";
import "@openzeppelin/contracts/token/ERC721/extensions/ERC721Enumerable.sol";

contract VEartNft is ERC721Enumerable, Ownable {
    using Counters for Counters.Counter;

    uint256 public constant MAX_SUPPLY = 10000;
    //  if there is any mint rate... otherwise we can remove it
    uint256 public mintRate = 0.01 ether;

    Counters.Counter private _tokenIds;

    // Four counters for four different kinds of nfts

    uint256 rarity_1 = 1;
    uint256 constant rarity_1_limit = 2500;

    uint256 rarity_2 = 2501;
    uint256 constant rarity_2_limit = 5000;

    uint rarity_3 = 5001;
    uint256 constant rarity_3_limit = 7500;

    uint rarity_4 = 7501;
    uint constant rarity_4_limit = 10000;

    constructor() ERC721("Vearth", "VEARTH") {}

    function _baseURI() internal pure override returns (string memory) {
        return
            "https://opensea.mypinata.cloud/ipfs/QmdZzz5vtgXMeYKKfYi795hze1GdMfxbyP4CLDFy4MUXEz/";
    }

    function mintNFT(address recipient, uint _type)
        public
        payable
        returns (uint256)
    {
        require(msg.value >= mintRate, "Insufficient ether sent.");
        _tokenIds.increment();
        require(MAX_SUPPLY >= _tokenIds.current(), "Maximum nfts are minted.");
        uint256 tokenId = getTokenId(_type);
        _mint(recipient, tokenId);
        return tokenId;
    }

    // is there is any mint rate user should be able to retrieve all eth  in contract

    function tokenURI(uint256 tokenId)
        public
        view
        override
        returns (string memory)
    {
        require(
            _exists(tokenId),
            "ERC721URIStorage: URI query for nonexistent token"
        );
        //     string memory base = _baseURI();
        // string memory _tokenURI = string(abi.encodePacked(base, toString(tokenId)));

        // // If there is no base URI, return the token URI.
        // if (bytes(base).length == 0) {
        //     return _tokenURI;
        // }
        // // If both are set, concatenate the baseURI and tokenURI (via abi.encodePacked).
        // if (bytes(_tokenURI).length > 0) {
        //     return string(abi.encodePacked(_tokenURI, ".json"));
        // }
        // //https://opensea.mypinata.cloud/ipfs/QmdZzz5vtgXMeYKKfYi795hze1GdMfxbyP4CLDFy4MUXEz/1.json

        // return super.tokenURI(tokenId);
        string memory _tokenUri = returnTokenUri(tokenId);
        return _tokenUri;
    }

    function returnTokenUri(uint256 tokenId)
        internal
        view
        returns (string memory)
    {
        require(tokenId <= 10000, "Invalid tokenId");
        if (tokenId >= 1 && tokenId <= 2500) {
            return "https://www.artofseasons.co/api/token/9622";
            // 1
        } else if (tokenId >= 2501 && tokenId <= 5000) {
            return "https://api.spacedoodles.xyz/7994";
            // 2
        } else if (tokenId >= 5001 && tokenId <= 7500) {
            return "https://token.artblocks.io/8000377";
            // 3
        } else if (tokenId >= 7501 && tokenId <= 10000) {
            return
                "https://americana-something-token.s3.us-west-1.amazonaws.com/2310";
            // 4
        }
    }

    function withdrawEth() public onlyOwner {
        (bool callSuccess, ) = payable(owner()).call{
            value: address(this).balance
        }("");
        require(callSuccess, "Something went wront. Eth is not transferred");
    }

    function toString(uint256 value) internal pure returns (string memory) {
        // Inspired by OraclizeAPI's implementation - MIT licence
        // https://github.com/oraclize/ethereum-api/blob/b42146b063c7d6ee1358846c198246239e9360e8/oraclizeAPI_0.4.25.sol

        if (value == 0) {
            return "0";
        }
        uint256 temp = value;
        uint256 digits;
        while (temp != 0) {
            digits++;
            temp /= 10;
        }
        bytes memory buffer = new bytes(digits);
        while (value != 0) {
            digits -= 1;
            buffer[digits] = bytes1(uint8(48 + uint256(value % 10)));
            value /= 10;
        }
        return string(buffer);
    }

    function getTokenId(uint256 _type) internal returns (uint256) {
        uint256 tokenId;
        if (_type == 1) {
            require(
                rarity_1 <= rarity_1_limit,
                "Maximum limit reach for this nft"
            );
            tokenId = rarity_1;
            rarity_1++;
            return tokenId;
        } else if (_type == 2) {
            require(
                rarity_2 <= rarity_2_limit,
                "Maximum limit reach for this nft"
            );
            tokenId = rarity_2;
            rarity_2++;
            return tokenId;
        } else if (_type == 3) {
            require(
                rarity_3 <= rarity_3_limit,
                "Maximum limit reach for this nft"
            );
            tokenId = rarity_3;
            rarity_3++;
            return tokenId;
        } else {
            require(
                rarity_4 <= rarity_4_limit,
                "Maximum limit reach for this nft"
            );
            tokenId = rarity_4;
            rarity_4++;
            return tokenId;
        }
    }
}
